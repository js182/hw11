let show = document.querySelector(".fa-eye");
let showTwo = document.querySelector(".fa-eye-slash");
let pwd = document.getElementById("pwd");
let pwdTow = document.getElementById("pwdTwo");
let btn = document.getElementById("btnConfirm");
let styleElem = document.head.appendChild(document.createElement("style"));

let pwdShown = 0;
function ShownOn() {
  pwd.setAttribute("type", "text");
  styleElem.innerHTML = `#switch:before {content: "\\f070"}`;
}
function Shownoff() {
  pwd.setAttribute("type", "password");
  styleElem.innerHTML = `#switch:before {content: "\\f06e"}`;
}
function ShownOnTwo() {
  pwdTow.setAttribute("type", "text");
  styleElem.innerHTML = `#switchTwo:before {content: "\\f070"}`;
}
function ShownoffTwo() {
  pwdTow.setAttribute("type", "password");
  styleElem.innerHTML = `#switchTwo:before {content: "\\f06e"}`;
}
show.addEventListener("click", function () {
  if (pwdShown == 0) {
    pwdShown = 1;
    ShownOn();
  } else {
    pwdShown = 0;
    Shownoff();
  }
});
showTwo.addEventListener("click", function () {
  if (pwdShown == 0) {
    pwdShown = 1;
    ShownOnTwo();
  } else {
    pwdShown = 0;
    ShownoffTwo();
  }
});

btn.addEventListener("click", function () {
  if (pwd.value === pwdTow.value) {
    alert(`с текстом - You are welcome`);
  } else {
    pwdShown = 0;
    console.log(document.querySelector(".error"));
    document.querySelector(".error").innerText =
      "Нужно ввести одинаковые значения";
  }
});
